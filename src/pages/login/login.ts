import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { LoginService } from "../../providers/login-service/login-service";
import { LoginModel } from "../../models/login";
import { ToastController, MenuController, Platform } from 'ionic-angular';
import { Validators, AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { UserModel } from "../../models/user";
import { HomePage } from '../../pages/home/home';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  // Form zum Validieren
  form: FormGroup;
  portal: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;

  // Benutzerdaten
  loginData: LoginModel = new LoginModel();

  // Wird gerade gearbeitet?
  working:Boolean=false;


  constructor(private navCtrl: NavController, private toastCtrl: ToastController,
    public loginService: LoginService, fb: FormBuilder, protected platform: Platform,
    public menu: MenuController) {

    this.initalizeForm(fb);

    this.menu.enable(false);

    // Falls ein Benutzer angemeldet ist, abmelden
    if (this.loginService.currentUser) {
      this.loginService.logOutUser();
    }

    // Für die Entwicklung
    // this.loginData.portal = 'http://product-stream.muench-its.de/backend'
    this.loginData.portal = 'http://localhost:3000'; 
    // this.loginData.portal = document.URL + "backend";

    this.loginData.email = 'jlampe@testmail.de';
    
    this.loginData.password = 'topsecret';
    

    if (!this.platform.is('core')) {
      this.loginData.password = 'topsecret';
    }
  }


  /**
   * Initalisiert das Formular
   */
  private initalizeForm(fb: FormBuilder): void {
    this.form = fb.group({
      'portal': ['', Validators.compose([Validators.required])],
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.portal = this.form.controls['portal'];
    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }


  /**
   * Routen Aufgrund des Benutzers in spezifische Komponenten
   * @param user 
   */
  private routeAfterLogin(user: UserModel): void {
    // Weiterleiten auf Startseite
    this.navCtrl.setRoot(HomePage);
  }

  /**
   * Anmelden am Backend
   */
  login() {
    console.log("LoginPage: Login gestartet.")
    this.working = true;
    this.loginService.logInUser(this.loginData).subscribe(
      res => {
        console.log("LoginPage: Benutzer erfolgreich authentifiziert.");
        this.showSuccess();
        this.working = false;
        this.routeAfterLogin(this.loginService.currentUser);
      },
      err => {
        console.log("LoginPage: Benutzer konnte nicht angemeldet werden.")
        console.log(err);
        this.working = false;
        this.showError();
      }
    )
  }

  /**
   * Anmeldung Erfolgreich Toast
   */
  showSuccess(): void {
    let toast = this.toastCtrl.create({
      message: 'Sie wurden erfolgreich angemeldet.',
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }

  /**
   * Fehlermeldung anzeigen
   */
  showError(): void {
    let toast = this.toastCtrl.create({
      message: 'Benutzer konnte nicht angemeldet werden. Bitte überprüfen Sie ihre Zugangsdaten.',
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
