import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MovieListModel } from '../../models/movie_list';
import { MovieServiceProvider } from '../../providers/model-services/movie-service';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  movies: MovieListModel[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private movieService: MovieServiceProvider) {
    this.initMovies()
  }

  getMovies() {
    this.initMovies()
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ListPage, {
      item: item
    });
  }

  initMovies() {
    this.movieService.all().subscribe(
      res => { this.movies = res; console.log(this.movies) },
      err => { console.log("Fehler: " + err) }
    )
  }
}
