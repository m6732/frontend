import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage

  currentPage: any;

  pages: Array<{
    title: string, visible: boolean, cssClass?: string,
    elements: Array<{title: string, color?: string, access?: boolean, icon?: string, component?: any }>
  }>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              private events: Events) {
    this.initializeApp();
    events.subscribe('user:loggedIn',(user) => {
      this.buildPages();
    })
  }

  private buildPages(): void {
    this.pages = [
      {
        title: null, visible: true, elements: [
          { title: 'Home', icon:'basket', component: HomePage, access: true },
          { title: 'List', icon:'link', component: ListPage, access: true },
        ]
      },
      {
        title: 'SIDEMENU.administration', visible: false, cssClass: 'bottom', elements: [
          // { title: 'SIDEMENU.profile', true, icon: "person", component: ProfilePage },
          // { title: 'SIDEMENU.settings', access: this.hasAccess('settings'), icon: "settings", component: SettingsPage },
          { title: 'SIDEMENU.sign_out', access: true, color: 'red', icon: "log-out", component: LoginPage },
          ]
      }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    if(this.currentPage){
      this.currentPage.color = "";
    }
    
    if(page.color == null)
      page.color = ""

    page.color = "active";

    this.currentPage = page;
    this.nav.setRoot(page.component);
  }
}
