import { Injectable } from "@angular/core";

/**
 * Dienst der Tools für Formulare anbietet
 */
@Injectable()
export class HelperService {

    /**
     * Entfernen von Ids
     */
    removeIdsFromObjects(object:any):void{
        this.removeIds(object);
    }

    /**
     * Entfernt rekursiv Ids aus allen Objekten
     * @param object 
     */
    private removeIds(object:any): void{
        Object.keys(object).forEach(prop => {
            if(prop.indexOf("id") >= 0){
                object[prop] = null;
            }
            else if(object[prop] && typeof(object[prop]) == "object"){
                this.removeIds(object[prop]);
            }
        });
    }
}