import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Angular2TokenService } from "angular2-token";
import { BasicModel } from "../../models/basic";
import { Response } from '@angular/http';
import { CacheService } from "./cache-service";
@Injectable()
/**
 * Dienst der das Profil eines Benutzers im Backend speichert
 */
export class DataServiceProvider<T extends BasicModel> extends CacheService<T> {

    endpoint: string = 'shouldBeOverwritten/';
    objectName: string = "shouldBeOverwritten";

    constructor(protected tokenService: Angular2TokenService) {
        super();
    }


    /**
     * Erzeugt ein Objekt zum versenden
     * @param obj 
     */
    private createHashForSend(obj: T): any {
        let hash = {};
        hash[this.objectName] = obj;
        return hash;
    }


    /**
     * Generiert ein neues Objekt aus dem Backend.
     * Notwendig für komplexe Objekte
     */
    new(): Observable<T> {
        return this.tokenService.get(this.endpoint + 'new')
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }


    /**
     * Löscht ein Objekt
     * @param obj 
     */
    destroy(obj: T): Observable<T> {
        return this.tokenService.delete(this.endpoint + obj.id)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }

    /**
     * Ruft alle Objekte des Types ab
     */
    all(): Observable<T[]> {
        return this.tokenService.get(this.endpoint)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }

    /**
     * Sucht Objekte mit Argumenten
     * @param args 
     */
    where(args): Observable<T[]> {
        return this.tokenService.get(this.endpoint, { params: args })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }


    /**
     * Speichert ein Objekt im Backend
     * @param obj 
     */
    save(obj: T): Observable<T> {
        if (!obj.id) {
            return this.create(obj);
        } else {
            return this.update(obj);
        }
    }

    /**
     * Ruft eine Objekt mit Details ab
     * @param id 
     */
    find(id): Observable<T> {
        return this.tokenService.get(this.endpoint + id)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    /**
     * Suchen mit Nutzung des Caches
     * @param id 
     */
    findWithCache(id): Observable<T> {
        
        let requestFunc = (id:number): Observable<T> => { return this.find(id); } ;
        return this.getCachedObject(id, requestFunc);
        
    }


    /**
     * Erstellen einer Objekt
     * @param obj
     */
    private create(obj: T): Observable<T> {
        return this.tokenService.post(this.endpoint, this.createHashForSend(obj))
            .map((res: Response) => res.json() as T)
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }

    /**
     * Aktualisieren einer Objekt
     * @param obj
     */
    private update(obj: T): Observable<T> {
        return this.tokenService.put(this.endpoint + obj.id, this.createHashForSend(obj))
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }

    /**
     * Ruft die aktuellen Benutzerdaten ab
     */
    get(id: number): Observable<T> {
        return this.tokenService.get(this.endpoint + id)
            .map((res: any) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }


}