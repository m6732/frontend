import { Injectable } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation/dist";
import { AlertController } from "ionic-angular/components/alert/alert-controller";

/**
 * Dienst der Tools für Formulare anbietet
 */
@Injectable()
export class FormService {

    constructor(private alertCtrl: AlertController) {

    }

    public countErrors(form: FormGroup): number {
        if (!form.valid) {
            let count: number = 0;
            Object.keys(form.controls).forEach(key => {
                let control = form.controls[key];
                if (!control.valid)
                    count++;
            });
            return count;
        }
        else
            return 0;
    }

    /**
     * Öffnen eiines Kommentar dialoges
     * @param fieldName 
     */
    public CommentDialog(fieldName: String, currentValue: String): Promise<any> {
        return new Promise((resolve, reject) => {

            let alert = this.alertCtrl.create({
                title: "Kommentar zu '" + fieldName + "' erfassen.",
                inputs: [
                    {
                        name: 'comment',
                        placeholder: 'Bitte tragen Sie hier ihren Kommentar ein',
                        value: currentValue ? currentValue.toString() : ''
                    },
                ],
                buttons: [
                    {
                        text: 'Abbrechen',
                        role: 'cancel',
                        handler: data => {
                            console.debug('Comment: Cancel clicked');
                            reject(data);
                        }
                    },
                    {
                        text: 'Übernehmen',
                        handler: data => {
                            resolve(data);
                        }
                    }
                ],
                /**
                 * Alert schließt nach "Esc" klicken nicht mehr
                 * Runtime Error wird somit ausgeschlossen
                 */
                enableBackdropDismiss: false
            });
            alert.present();
        });
    }

    /**
     * Rundund auf Nachkommastellen
     */
    public precisionRound(number: number, precision: number): number {
        var factor = Math.pow(10, precision);
        return Math.round(number * factor) / factor;
    }

    /**
     * Erstellt wert auf zwei nachkommastellen
     * @param input 
     */
    public roundTwoDecimals(input: string): string {
        if (input != null) {
            let vTemp = input.toString().replace(".", "").replace(",", ".");
            let v: number = parseFloat(vTemp);
            return isNaN(v) ? null : v.toFixed(2);
        }
        else
            return null;
    }

    /**
     * Gibt ein FormControl zurück zum Valdiieren von Telefon-Nummern
     */
    public phoneFormControl(): FormControl {
        return new FormControl('', Validators.pattern("[0-9+]+-[0-9]+"));
    }

    /**
     * Form-Control welche benötigt wird
     */
    public requiredFormControl(): FormControl {
        return new FormControl('', Validators.required);
    }

    /**
     * Optionales Form, Jedoch größer 0
     */
    public greaterZeroFormControl(): FormControl {
        return new FormControl('', CustomValidators.min(0));
    }


    /**
     * Pflicht Form, Jedoch größer 0
     */
    public greaterZeroFormControlAndRequired(): FormControl {
        return new FormControl('', Validators.required, CustomValidators.min(0));
    }
}