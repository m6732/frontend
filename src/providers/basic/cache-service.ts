import { Observable } from "rxjs";
import { BasicModel } from "../../models/basic";

// Dient zum Cachen von Objekten
export class CacheService<T extends BasicModel> {

    private objectCache: any = {};

    /**
     * Probiert ein Objekt aus dem Cache abzurufen
     * @param id 
     */
    protected getCachedObject(id: any, requestFunc: (n: any) => Observable<T>): Observable<T> {
        let obj = this.objectCache[id];

        if (obj) {
            return Observable.create(observer => {
                observer.next(obj);
                observer.complete();
            });
        }
        else {
            let request = requestFunc(id);
            this.cacheObject(id, request);
            return request;
        }
    }

    /**
     * Cached ein Objekt direkt
     * @param id 
     * @param value 
     */
    protected cacheDirect(id:any, value: any){
        this.objectCache[id] = value;
    }

    /**
     * Cached ein Objekt
     * @param id 
     * @param request 
     */
    protected cacheObject(id: any, request: Observable<T>): void {
        request.subscribe(
            res => {
                this.cacheDirect(id, res);
            }
        )
    }
}