import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { LoginModel } from "../../models/login";
import { UserModel } from "../../models/user";
import { Angular2TokenService } from "angular2-token";
import { Events } from "ionic-angular";
import { Response } from '@angular/http';

@Injectable()
export class LoginService {

    userSignedIn$: Subject<boolean> = new Subject();
    currentUser: UserModel;
    currentPortal: string;

    constructor(public authService: Angular2TokenService, public events: Events) {
    }

    /**
     * Init AuthService
     */
    initAuthService(portal: string): void {
        this.currentPortal = portal;
        this.authService.init({
            apiBase: portal,
        });
    }

    // /**
    //  * Überprüft ob der aktuelle Bentuzer diese Berechtigung hat
    //  * @param right
    //  */
    // hasAccess(right: string): boolean {
    //     if (!this.currentUser)
    //         return false

    //     let foundRight = this.currentUser.rights.find(r => r.key === right)
    //     return foundRight ? foundRight.active : false;
    // }

    /**
     * Gibt die aktuelle URL des verbundenen Portals zurück
     */
    getPortal(): string {
        return this.currentPortal;
    }

    /**
     * Gibt den aktuellen Benutzer zurück
     */
    getCurrentUser(): UserModel {
        return this.currentUser;
    }

    /**
     * Setzt den akuellen Benutzer
     * @param user
     */
    setCurrentUser(user:UserModel): void {
        this.currentUser = user;
    }

    /**
     * Abmelden des aktuellen Benutzers
     */
    logOutUser(): Observable<Response> {
        
        return this.authService.signOut().map(
            res => {
                this.userSignedIn$.next(false);
                return res;
            }
        );
    }

    /**
     * Anmelden des Benutzers am Backend
     * @param loginData
     */
    logInUser(loginData: LoginModel): Observable<Object> {

        // Anmeldung am Backend, Initialisierung beim ersten Call
        this.initAuthService(loginData.portal);

        // Start der Authentifizierung
        return this.authService.signIn({ email: loginData.email, password: loginData.password }).map(
            res => {
                this.currentUser = res.json().data;
                this.events.publish('user:loggedIn', this.currentUser);
                this.events.publish('user:loggedIn', this.currentUser);
                this.userSignedIn$.next(true);

                return res;
            }
        );

    }


}