import { Injectable } from '@angular/core';
import { DataServiceProvider } from "../basic/data-service";
import { MovieListModel } from '../../models/movie_list';

@Injectable()
/**
 * Dienst der Verwendungszwecke abruft
 */
export class MovieServiceProvider extends DataServiceProvider<MovieListModel> {
  endpoint: string = '/movies/'
  objectName: string = "movie";
}
