import { UserData } from "ionic-token-auth";
import { BasicModel } from "./basic";

/**
 * UserModel
 * ----------
 * Repräsentiert einen Benutzer
 */
export class UserModel extends BasicModel implements UserData {

    // constructor(hash) {
    //     super();
    //     for (var prop in hash) {
    //         this[prop] = hash[prop];
    //     }
    // }


    email: string;
    provider: string;
    uid: string;
    name: string;
    nickname: string;
    image: any;

    firstname: string;
    lastname: string;
    street: string;
    zip: string;
    city: string;
    phone: string;
    fax: string;
    language: string;
 
    active:boolean;
    ready:boolean;
    should_change_password:boolean;

    company: string;
    website: string;

    password: string;
    password_confirmation: string;
}