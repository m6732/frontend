import { BasicModel } from "./basic";

export class MovieModel extends BasicModel {
    id: number;
    vote_average: number;
    title: string;
    popularity: number;
    poster_path: string;
    original_language: string;
    original_title: string;
    adult: boolean;
    overview: string;
    release_date: string;
    // genre: GenreModel
}