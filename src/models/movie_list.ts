import { BasicModel } from "./basic";
import { MovieModel } from "./movie";

export class MovieListModel extends BasicModel {
    page: number;
    total_results: number;
    total_pages: number;
    results: MovieModel[];
}