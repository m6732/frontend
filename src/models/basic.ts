/**
 * BasisModel
 * dient zur Abstraktion im dataService
 */
export class BasicModel {
    id: number;
    _destroy: Boolean;
    created_at: Date;
    updated_at: Date;

    /**
     * Löschen eines Objektes aus einer Aufzählung
     * @param objects 
     * @param obj 
     */
    static delete(objects: BasicModel[], obj: BasicModel): void {
        if (obj.id) {
            obj._destroy = true;
        }
        else {
            let index = objects.indexOf(obj);
            objects.splice(index, 1);
        }
    }
}