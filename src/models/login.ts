/**
 * LoginModel
 * ----------
 * Wird genutzt zur Authentifizierung
 */
export class LoginModel {
    portal: string;
    email: string;
    password: string;
}